import getpass
import os
import sys

# Returns the parameter from the specified file
def get_parameter( parameter, file_path ):
    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("File %s not found, asking."%file_path)
        write_parameter( parameter, file_path )
        #sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

def write_parameter( parameter, file_path ):
    if not os.path.exists('config'):
        os.makedirs('config')
    print("Setting up Twitter keys & tokens ...")
    print("\n")
    api_key = input("API Key: ")
    api_key_secret = input("API Key Secret: ")
    access_token = input("Access Token: ")
    access_token_secret = input("Access Token Secret: ")
    
    with open(file_path, "w") as text_file:
        print("api_key: {}".format(api_key), file=text_file)
        print("api_key_secret: {}".format(api_key_secret), file=text_file)
        print("access_token: {}".format(access_token), file=text_file)
        print("access_token_secret: {}".format(access_token_secret), file=text_file)
    
    print('\nDone!')

###############################################################################
# main

if __name__ == '__main__':

    # Load configuration from config file
    twitter_config_filepath = "config/keys_config.txt"
    api_key = get_parameter("api_key", twitter_config_filepath)
    api_key_secret = get_parameter("api_key_secret", twitter_config_filepath)
    access_token = get_parameter("access_token", twitter_config_filepath)
    access_token_secret = get_parameter("access_token_secret", twitter_config_filepath)
