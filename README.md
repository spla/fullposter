# fullposter
Publish all your public Mastodon posts to Twitter and all your Twitter ones to Mastodon!  
fullposter will replicate your public posts and threads of any of these two social networks to the other one, automagically.  
fullposter respect your privacy because it stores your Mastodon and Twitter access token in your Linux or Raspberry box. By using fullposter your are not giving away your access tokens to third parties.
Run fullposter in any Linux box or even in your Raspberry!

### Dependencies

-   **Python 3**
-   Postgresql server
-   Mastodon user account  
-   Twitter user account
-   Your personal Linux or Raspberry box.

### Usage:

Within Python Virtual Environment:

1. Run `pip install -r requirements.txt` to install needed libraries.

2. Run `python db-setup.py` to setup and create new Postgresql database and needed table in it and setup your Mastodon's account RSS feed in the format 'https://your.mastodon.server/@your_user.rss'

3. Run `python setup.py` to input and save your Twitter's key and access tokens. You can get your keys and tokens from [Twitter Developer Platform](https://developer.twitter.com/en/apply/user.html)  

4. Run `python mastodon-setup.py` to setup your Mastodon account access tokens.

5. Use your favourite scheduling method to set `python fullposter.py` to run every minute.  

17.11.2021 Upgraded Tweepy library to 4.3.0 version.  
17.11.2021 **New Feature** Added polls support!

