import os
import feedparser
from bs4 import BeautifulSoup
from mastodon import Mastodon
import psycopg2
import sys
import time
import requests
import shutil
import tweepy
from tweepy.errors import (
    BadRequest, Forbidden, HTTPException, TooManyRequests, TwitterServerError,
    Unauthorized
)
from tweepy import TweepyException
import logging
import filetype
import ffmpeg
import pdb

logger = logging.getLogger()

def get_poll(tuit_text):

    poll_substring = '[ ]'
    poll_options = tuit_text.count(poll_substring)

    is_poll = False if poll_options == 0 else True

    options_lst = []

    remain_str = tuit_text.replace('\n', '')

    i = poll_options
    while (i > 0):

        last_option_index = remain_str.rfind('[ ]')
        option_str = remain_str[last_option_index+3:].strip()
        options_lst.append(option_str)
        remain_str = remain_str[:last_option_index]
        i-=1

    if is_poll:

        options_lst_copy = options_lst.copy()
        options_lst_copy.reverse()
        options_lst = options_lst_copy.copy()
        tuit_text = remain_str

    return (tuit_text, poll_options, options_lst, is_poll)

def text_with_newlines(elem):

    text = ''

    for e in elem.recursiveChildGenerator():

        if isinstance(e, str):

            text += f'{e}'

        elif e.name == 'br':

            text += '\n'

    return text

def get_toot(title):

    toot_text = ''
    soup = BeautifulSoup(title, 'html.parser')

    toot_text = text_with_newlines(soup)

    sub_str = 'http'
    find_link = toot_text.find(sub_str)
    if find_link != -1:

        tuit_text = toot_text[:toot_text.index(sub_str)]

    else:

        tuit_text = toot_text

    links_lst = ''
    for links in soup.find_all('a'):
        find_tag = links.get('href').find('/tags/')
        if find_tag == -1:
            links_lst += links.get('href')

    if len(links_lst) > 0:

        last_text = toot_text[len(tuit_text) + len(links_lst):]

    else:

        last_text = ''

    tuit_text = f'{tuit_text} {links_lst} {last_text}'

    tuit_text, poll_options, options_lst, is_poll = get_poll(tuit_text)

    return (tuit_text, poll_options, options_lst, is_poll)

def compose_poll(tuit_text, poll_options, options_lst, toot_id):

    try:

        tweet = apiv2.create_tweet(
            poll_duration_minutes=4320,
            poll_options = options_lst,
            text=tuit_text
        )

        write_db(toot_id, tweet.data['id'])

    except TweepyException as err:

        print('Error while trying to publish poll.\n')
        sys.exit(err)

    return tweet

def compose_tweet(tuit_text, with_images, is_reply, toot_id):

    images_id_lst = []

    if with_images:

        for image in images_list:

            kind = filetype.guess('images/' + image)
            is_video = True if kind.mime == 'video/mp4' else False

            if is_video:

                try:

                    probe = ffmpeg.probe(f'images/{image}')
                    video_duration = float(
                        probe['streams'][0]['duration']
                    )

                except Exception as e:

                    print(f'Error while trying to probe {image}\n{e}')
                    sys.exit(e)

                if video_duration > 139:

                    print(f'video duration is too large: {video_duration}')
                    continue

            try:

                media_upload = apiv1.media_upload(
                    f'images/{image}',
                    media_category='tweet_video' if is_video else 'tweet_image'
                )

                if is_video:

                    if media_upload.processing_info['state'] == 'succeeded':

                        images_id_lst.append(media_upload.media_id)

                else:

                    images_id_lst.append(media_upload.media_id)

            except TweepyException as err:

                print('Error while uploading media!\n')
                sys.exit(err)

    # Compose tuit
    
    tuit_text2 = ''

    three_parts = False

    if len(tuit_text) > 280:

        tuit_max_length = 250 if with_images else 273

        tuit_text1 = '{0}...'.format(tuit_text[:tuit_max_length].rsplit(' ', 1)[0])
        
        tuit_text2 = '{0}'.format(tuit_text[len(tuit_text1) - 2:])
        
        try:

            first_tweet = apiv1.update_status(
                status=tuit_text1,
                in_reply_to_status_id=tweet_id if is_reply else '',
                media_ids=images_id_lst
            )

            write_db(toot_id, first_tweet.id)

            tweet = apiv1.update_status(
                status=tuit_text2,
                in_reply_to_status_id=first_tweet.id
            )
            
            write_db(toot_id, tweet.id)

        except TweepyException as err:

            print('Error while trying to publish split tweet.\n')
            sys.exit(err)

    else:

        try:

            tweet = apiv1.update_status(
                status=tuit_text,
                in_reply_to_status_id=tweet_id if is_reply else '',
                media_ids=images_id_lst
            )
            write_db(toot_id, tweet.id)

        except TweepyException as err:

            print('Error while trying to publish tweet.\n')
            sys.exit(err)

    return tweet

def get_tweet_id(toot_id):

    tweet_id = 0

    try:

        conn = None

        conn = psycopg2.connect(database = feeds_db, user = feeds_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()

        cur.execute('select tweet_id from id where toot_id=(%s)', (toot_id,))

        row = cur.fetchone()

        if row != None:

            tweet_id = row[0]

        cur.close()

        return(tweet_id)

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

def get_toot_id(tweet_id):

    toot_id = 0

    try:

        conn = None

        conn = psycopg2.connect(database = feeds_db, user = feeds_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()

        cur.execute('select toot_id, tweet_id from id where tweet_id=(%s)', (tweet_id,))

        row = cur.fetchone()

        if row != None:

            toot_id = row[0]

        cur.close()

        return(toot_id)

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

def write_db(toot_id, tweet_id):

    sql_insert_ids = 'INSERT INTO id(toot_id, tweet_id) VALUES (%s,%s)'

    conn = None

    try:

        conn = psycopg2.connect(database = feeds_db, user = feeds_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()

        cur.execute(sql_insert_ids, (toot_id, tweet_id))

        conn.commit()

        cur.close()

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

def write_image(image_url):

    if not os.path.exists('images'):
        os.makedirs('images')
    filename = image_url.split("/") [-1]
    r = requests.get(image_url, stream = True)
    r.raw.decode_content = True
    with open('images/' + filename, 'wb') as f:
        shutil.copyfileobj(r.raw, f)

    return filename

def create_api_v1():
    
    auth = tweepy.OAuthHandler(api_key, api_key_secret)
    auth.set_access_token(access_token, access_token_secret)
    apiv1 = tweepy.API(auth)
    try:
        apiv1.verify_credentials()
        logged_in = True
    except Exception as e:
        logger.error("Error creating API", exc_info=True)
        raise e
    logger.info("API created")
    return (apiv1, logged_in)

def create_api_v2():

    try:

        apiv2 = tweepy.Client(
            consumer_key=api_key,
            consumer_secret=api_key_secret,
            access_token=access_token,
            access_token_secret=access_token_secret
            )
        logged_in = True

    except Exception as e:

        logger.error("Error creating API", exc_info=True)
        raise e

    logger.info("API v2 created")

    return (apiv2, logged_in)

def mastodon():

    # Load secrets from secrets file
    secrets_filepath = "secrets/secrets.txt"
    uc_client_id = get_parameter("uc_client_id",     secrets_filepath)
    uc_client_secret = get_parameter("uc_client_secret", secrets_filepath)
    uc_access_token = get_parameter("uc_access_token",  secrets_filepath)

    # Load configuration from config file
    config_filepath = "config/config.txt"
    mastodon_hostname = get_parameter("mastodon_hostname", config_filepath)

    # Initialise Mastodon API
    mastodon = Mastodon(
        client_id=uc_client_id,
        client_secret=uc_client_secret,
        access_token=uc_access_token,
        api_base_url='https://' + mastodon_hostname,
    )

    # Initialise access headers
    headers = {'Authorization': 'Bearer %s'%uc_access_token}

    return (mastodon, mastodon_hostname)

def db_config():

    # Load db configuration from config file
    db_config_filepath = "config/db_config.txt"
    feeds_db =  get_parameter("feeds_db", db_config_filepath)
    feeds_db_user =  get_parameter("feeds_db_user", db_config_filepath)
    feeds_url =  get_parameter("feeds_url", db_config_filepath)
    
    return (feeds_db, feeds_db_user, feeds_url)

def twitter_config():
    
    twitter_config_filepath = "config/keys_config.txt"
    api_key = get_parameter("api_key", twitter_config_filepath)
    api_key_secret = get_parameter("api_key_secret", twitter_config_filepath)
    access_token = get_parameter("access_token", twitter_config_filepath)
    access_token_secret = get_parameter("access_token_secret", twitter_config_filepath)

    return(api_key, api_key_secret, access_token, access_token_secret)

def get_parameter( parameter, file_path ):
    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("File %s not found, exiting."%file_path)
        sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

# main

if __name__ == '__main__':

    mastodon, mastodon_hostname = mastodon()

    feeds_db, feeds_db_user, feeds_url = db_config()

    api_key, api_key_secret, access_token, access_token_secret = twitter_config()

    logged_in = False

    try:

        newsfeeds = feedparser.parse(feeds_url)

    except:

        print(newsfeeds.status)
        sys.exit(0)

    for entry in newsfeeds.entries:

        publish = False
        with_images = False
        is_reply = False

        title = entry['summary']
        id = entry['id']
        link = entry['link']

        toot_id = link.rsplit('/')[4]

        tweet_id = get_tweet_id(toot_id)

        if tweet_id == 0:

            publish = True

            reply_id = mastodon.status(toot_id).in_reply_to_id
            if reply_id != None:

                is_reply = True
                tweet_id = get_tweet_id(reply_id)

            if len(entry.links) >= 2:

                with_images = True
                images_list = []
                images = len(entry.links) - 1

                i = 0
                while i < images:

                    image_url = entry.links[i+1].href
                    image_filename = write_image(image_url)
                    images_list.append(image_filename)
                    i += 1

        ###########################################################

        if publish:

            tuit_text, poll_options, options_lst, is_poll = get_toot(title)

            print("Tooting...")
            print(tuit_text)

            if not logged_in:

                apiv1, logged_in = create_api_v1()
                apiv2, logged_in = create_api_v2()

            if is_poll:

                tweet = compose_poll(tuit_text, poll_options, options_lst, toot_id)

            else:

                tweet = compose_tweet(tuit_text, with_images, is_reply, toot_id)

            time.sleep(2)

    # check new tweets

    if not logged_in:

        apiv1, logged_in = create_api_v1()

    user_tweets = apiv1.user_timeline(tweet_mode='extended')

    for tweet in reversed(user_tweets):

        if not tweet.retweeted and tweet.entities['user_mentions'] == []:

            tweet_id = tweet.id
            toot_id = get_toot_id(tweet_id)

            if toot_id == 0:

                print(f'tweet id: {tweet.id}, {tweet.full_text}')

                is_reply = False

                is_media = False

                toot_text = tweet.full_text

                if tweet.in_reply_to_status_id != None:

                    tweet_reply_id = tweet.in_reply_to_status_id

                    reply_toot_id = get_toot_id(tweet_reply_id)

                    is_reply = True

                else:

                    reply_toot_id = None

                if 'media' in tweet.entities:

                    is_media = True

                    toot_text = toot_text.replace(tweet.entities['media'][0]['url'],'')

                    images_id_lst = []
                    media_qty = len(tweet.entities['media'])

                    i = 0
                    while i < media_qty:

                        media_url = tweet.entities['media'][0]['media_url']
                        image_filename = write_image(media_url)
                        image_id = mastodon.media_post('images/'+ image_filename, "image/png", description='twitter image').id
                        images_id_lst.append(image_id)

                        i += 1

                toot_id = mastodon.status_post(toot_text, in_reply_to_id=reply_toot_id, media_ids=images_id_lst if is_media else None).id

                write_db(toot_id, tweet_id)

                time.sleep(2)

    print('Done.')
